#encoding: UTF-8
Feature: ID: 156059449 Name: Photo Gallery: "Fullscreen" link

Background: User is Logged In
  Given the user has been logged in as a buyer
  And the buyer has visited a detail vehicle page with the "https://www.edgepipeline.com/components/vehicle/detail/bigvalleyaargv/208551" url

Scenario: There are photos for the vehicle
  Given a web browser is on "https://www.edgepipeline.com/components/vehicle/detail/bigvalleyaargv/208551" page
  When the user clicks the "Fullscreen" link
  Then the selected photo is shown in Fullscreen mode

Scenario: Previous photo is displayed when I click the back arrow
  Given a photo is displayed in Fullscreen mode
  When the user clicks the back arrow
  Then the previous photo is displayed as main photo

Scenario: Next photo is displayed when I click the forward arrow
  Given a photo is displayed in Fullscreen mode
  When the user clicks the forward arrow
  Then the next photo is displayed as main photo

Scenario: Exiting Fullscreen mode
  Given a photo is displayed in Fullscreen mode
  When the user clicks anywhere except the navigation arrows
  Then Fullscreen view is minimized