#encoding: UTF-8

When(/^the user clicks the "Fullscreen" link$/) do
  @img_src = find('.fotorama__active.fotorama__stage__frame .fotorama__img')['src']
  fullscreen_link = find('.fullscreen-slideshow')
  search_again = false

  fullscreen_link.click
end

Then(/^the selected photo is shown in Fullscreen mode$/) do
  within('.fotorama__stage .fotorama__stage__shaft') do
    fullscreen_photo_src = find('.fotorama__active .fotorama__img')['src']

    expect(fullscreen_photo_src).to match(@img_src)
  end
end

Given(/^a photo is displayed in Fullscreen mode$/) do
  fullscreen_link = find('.fullscreen-slideshow')
  search_again = false

  fullscreen_link.click
end

When(/^the user clicks the back arrow$/) do
  sleep 2
  within(".fotorama__nav-wrap") do
    @target_img = find('.fotorama__nav__shaft .fotorama__nav__frame--thumb:last-child img')['src'][-15..-1]
  end

  find('.fotorama__arr.fotorama__arr--prev').click
end

Then(/^the previous photo is displayed as main photo$/) do
  sleep 2
  within('.fotorama__stage .fotorama__stage__shaft') do
    expect(find('.fotorama__active .fotorama__img')['src'][-15..-1]).to eq(@target_img)
  end
end

When(/^the user clicks the forward arrow$/) do
  sleep 2
  within(".fotorama__nav-wrap") do
    @target_img = find('.fotorama__nav__shaft .fotorama__nav__frame--thumb:nth-child(3) img')['src'][-15..-1]
  end

  find('.fotorama__arr.fotorama__arr--next').click
end

Then(/^the next photo is displayed as main photo$/) do
  sleep 2
  within('.fotorama__stage .fotorama__stage__shaft') do
    expect(find('.fotorama__active .fotorama__img')['src'][-15..-1]).to eq(@target_img)
  end
end

When(/^the user clicks anywhere except the navigation arrows$/) do
  find('.fotorama__stage__frame.fotorama__loaded.fotorama__loaded--img.fotorama__active').click
end

Then(/^Fullscreen view is minimized$/) do
  page.should have_no_css('div[class*="fotorama no-print fotorama1525789601276 fotorama--fullscreen"]')
end