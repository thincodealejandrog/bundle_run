Then("the vehicle main picture should be displayed with src attribute of {string}") do |url|
	within(:xpath, '//div[@class="fotorama__stage__shaft fotorama__grab"]') do 
		img_element = first(:xpath, './/img[@class="fotorama__img"]')
		if img_element.visible?
		else
			raise 'Main picture is not visible'
		end
		if img_element['src'] == url
		else
			raise 'The url of the found element is different from the url for the main image'
		end
	end
end

Given("pictures were provided") do
	imgs_count = 0
	within(:xpath, '//span[@class="slide-counts"]') do
		imgs_count = find(:xpath, './/span[@class="current-slide-number"]').text.to_i
	end
	if imgs_count > 0
	else
		raise 'No pictures were provided for this vehicle'
	end
end

Then("a thumbnail for each picture is shown") do

	imgs_count = 0

	within(:xpath, '//span[@class="slide-counts"]') do
		imgs_count = find(:xpath, './/span[@class="current-slide-number"]/following-sibling::span').text.to_i
	end

	thumbnails = all(:css,'div.fotorama__nav__frame', visible: false)

	if thumbnails.size == imgs_count
	else
		raise "The number of thumbnails pictures doesn't match the number of slide-counts"
	end

end

When("the buyer clicks on the main picture") do
	find(:xpath, '//div[@class="gallery"]').click
end

Then("the main picture is open to fullscreen") do
	within(:xpath, '//div[@class="gallery"]') do
		begin
			find(:xpath, './/div[@data-auto="false"]')
			raise "Main picture was not opened to fullscreen"
		rescue Capybara::ElementNotFound
		end
	end
	 #click picture in fullscreen to be able to find the logout section to logout
	 find(:xpath, '//div[@class="fotorama__stage__shaft fotorama__grab"]').click
end



