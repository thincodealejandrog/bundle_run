When(/^no photos are available for the vehicle$/) do
  gallery = find(".gallery.empty.no-thumbs")
end

Then(/^a Gray Camera image is displayed as main picture$/) do
  gallery = find(".gallery.empty.no-thumbs")

  gallery.should have_css('img[src*="/images/no-image.png"]')
end

And(/^'Fullscreen', 'Slideshow' and 'Download Pic' links are disabled$/) do
  gallery = find(".gallery.empty.no-thumbs")

  gallery.should have_css('a[class*="fullscreen-slideshow disabled"]')
  gallery.should have_css('a[class*="download-pic disabled"]')
  gallery.should have_css('a[class*="play-pause-slideshow disabled"]')
end