Given("the photo gallery has been displayed the link Slideshow with the play icon") do
  expect(find(:css, 'div.gallery')).to have_content('Slideshow')
  within("div.gallery") do
    find(:css, 'a.play-pause-slideshow')
    find(:css, 'i.fa-play')
  end
end

When("the buyer click on the Slideshow link") do
  find(:css, 'a.play-pause-slideshow').click
end

Then("the photo gallery trigger the photos to play on a {int} sec delay") do |int|
  within(:css, 'div.fotorama__stage') do
    within(:css, 'div.fotorama__active') do
      img_src = find(:css, 'img.fotorama__img')['src']
      #wait for the change of the image in the active fotorama
      sleep int+1
      expect(img_src).not_to have_content(find(:css, 'img.fotorama__img')['src'])
    end  
  end
end

Given("the photo gallery has been playing the photos on a {int} sec delay") do |int|
  expect(find(:css, 'div.gallery')).to have_content('Slideshow')
  find(:css, 'a.play-pause-slideshow').click
  within(:css, 'div.fotorama__stage') do
    within(:css, 'div.fotorama__active') do
      img_src = find(:css, 'img.fotorama__img')['src']
      #wait for the change of the image in the active fotorama
      sleep int+1
      expect(img_src).not_to have_content(find(:css, 'img.fotorama__img')['src'])
    end  
  end
end

And("the photo gallery has been displayed the link Slideshow with the pause icon") do  
  within("div.gallery") do
    find(:css, 'a.play-pause-slideshow')
    find(:css, 'i.fa-pause')
  end
end

Then("the photo gallery trigger the photos to stop") do
  within(:css, 'div.fotorama__stage') do
    within(:css, 'div.fotorama__active') do
      img_src = find(:css, 'img.fotorama__img')['src']
      #wait 6 seconds to validate that the img src doesnt change
      sleep 6
      expect(img_src).to have_content(find(:css, 'img.fotorama__img')['src'])
    end  
  end
end

Then("the slideshow icon link toggle to the pause icon") do
  within("div.gallery") do
    find(:css, 'a.play-pause-slideshow')
    find(:css, 'i.fa-pause')
  end
end
