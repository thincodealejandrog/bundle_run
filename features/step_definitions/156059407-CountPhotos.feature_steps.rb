Given("the seller of the vehicle has uploaded at least one photo for the vehicle") do
  find(:css, 'div.gallery')
  within(:css, 'div.fotorama__stage') do
    within(:css, 'div.fotorama__active') do
      expect(find(:css, 'img.fotorama__img')['src']).not_to eql('https://www.edgepipeline.com/images/no-image.png')
    end
  end
end

When("the photo gallery is loaded") do
  find(:css, 'div.gallery')
end

Then("the photo gallery shows the current photo numeric order") do  
  index_active_thumbnail = 0
  thumbnails = all(:css,'div.fotorama__nav__frame', visible: false)
  thumbnails.each_with_index do |thumbnail, index|
    if thumbnail.matches_css?('.fotorama__active', wait: 1)
      index_active_thumbnail = index + 1
      break
    end
  end
  within(:css, 'div.gallery') do
    within(:css, 'span.slide-counts') do
      expect(find(:css, 'span.current-slide-number')).to have_content(index_active_thumbnail)
    end
  end
end

And("the photo gallery shows the total number of photos") do
  thumbnails = all(:css,'div.fotorama__nav__frame', visible: false)
  within(:css, 'div.gallery') do
    within(:css, 'span.slide-counts') do
      expect(find(:css, '.slide-counts span:last-child')).to have_content(thumbnails.count())
    end
  end
end

And("the numeric order and the total number of photos are separated with the word {string}") do |string|
  count_photos = find(:css, 'span.slide-counts').text.split(" ")  
  expect(count_photos[1]).to have_content(string)
end

Given("the seller of the vehicle hasnt uploaded any photo") do
  find(:css, 'div.gallery')
  within(:css, 'div.fotorama__stage') do
    within(:css, 'div.fotorama__active') do
      expect(find(:css, 'img.fotorama__img')['src']).to eql('https://www.edgepipeline.com/images/no-image.png')
    end
  end
end

Then("the photo count shows {string}") do |string|
  expect(find(:css, 'span.slide-counts').text).to have_content(string)
end

Given("the photo gallery has been showing a thumbnail for each picture") do
  thumbnails = all(:css,'div.fotorama__nav__frame', visible: false)
  expect(find(:css, '.slide-counts span:last-child')).to have_content(thumbnails.count())
end

When("the buyer clicks on the second thumbnail") do
  thumbnails = all(:css,'div.fotorama__nav__frame', visible: false)
  thumbnails[1].click
end

