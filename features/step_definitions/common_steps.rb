﻿Given(/^I visited a vehicle page with photos to show$/) do
  visit "https://www.edgepipeline.com/components/vehicle/detail/bigvalleyaargv/208551"
  maximize_window
end

Given(/^I visited a vehicle page with no photos to show$/) do
  visit "https://www.edgepipeline.com/components/vehicle/detail/aaanorthhouston/293394?btr=%2Fcomponents%2Freport%2Fbuy_now%2Fview%2Fall%23vehicle_708_243246"
  maximize_window
end

Given("the user has been logged in as a buyer") do
  page.driver.browser.manage.window.resize_to(1280,720)
  visit('https://www.edgepipeline.com/components/login')
  fill_in('username', with: 'intellek')
  fill_in('password', with: '354035as')
  click_button('Sign In')
  expect(page).to have_current_path('https://www.edgepipeline.com/dashboard')
  expect(find(:css, 'span.full-name')).to have_no_content('Guess')
end

And("the buyer has visited a detail vehicle page with the {string} url") do |url|
  visit url
  expect(page).to have_current_path(url)
end
