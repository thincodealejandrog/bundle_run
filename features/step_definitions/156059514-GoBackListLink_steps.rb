# encoding: UTF-8
Given(/^a web browser is on "(.*)" page$/) do |page_url|
  maximize_window
  visit page_url
  @current_url = page_url
end

When(/^the "btr" query string is in the page url$/) do
  expect(@current_url.index(/btr=/)).not_to be(nil)
end

Then(/^the "(.*)" link appears$/) do |linkText|
  backlink_div = find('.backlink')
  backlink_div.should have_content(linkText)
end

# Should go to "All Listings" page
And(/^the "Go Back to List" link have the 'btr' query string value as its href attr$/) do
  require 'uri'
  
  main_url = "https://www.edgepipeline.com"
  # Retrive btr argument from page url
  starting_ind = @current_url.index(/btr=/)
  target_url = main_url + URI.unescape(@current_url[starting_ind+4..-1])

  expect(find('.backlink a')['href']).to eq(target_url)
end

When(/^the "btr" query string is not in the page url$/) do 
  page_url = URI.parse(current_url).to_s
  expect(page_url.index(/btr=/)).to be(nil)
end

Then(/^the "([^"]*)" does not appear$/) do |linkText|
  page.should have_no_content(linkText)
end
