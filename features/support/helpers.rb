module Helpers
  def without_resynchronize
    page.driver.options[:resynchronize] = false
    yield
    page.driver.options[:resynchronize] = true
  end
  def maximize_window
    page.driver.browser.manage.window.resize_to(1280, 800)
  end
end

World(Helpers)