After do
  Capybara.current_session.driver.quit
end

After('@buyer_log_out') do

	find(:xpath, '//span[@class="full-name"]').click


	within(:xpath, '//div[@class="menu-wrapper hide-on-small-only"]') do
		find(:xpath, '//a[@href=\'/components/login\']').click
	end

	still_logging_out = true
	while still_logging_out
		if find(:xpath, '//span[@class="full-name"]').text =~ /\s*Guest\s*/
			still_logging_out = false
		else
		end
	end
	
end