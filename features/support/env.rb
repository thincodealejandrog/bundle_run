require "capybara/cucumber"
require "rspec"
require "selenium-webdriver"

Capybara.default_max_wait_time = 20

Capybara.register_driver :safari do |app| 
  Capybara::Selenium::Driver.new(app, :browser => :safari)
end

Capybara.default_driver = :safari
