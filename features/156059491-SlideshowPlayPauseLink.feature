@buyer_log_out
@PivotalID_156059491 @PhotoGallery
Feature: ID-156059491 Name-Photo Gallery - Slideshow - play/pause link

  Background:
    Given the user has been logged in as a buyer
		And the buyer has visited a detail vehicle page with the 'https://www.edgepipeline.com/components/vehicle/detail/abcbirmingham/319097' url

  Scenario: Photo gallery plays the photos of the vehicle
    Given the photo gallery has been displayed the link Slideshow with the play icon
    When the buyer click on the Slideshow link
    Then the photo gallery trigger the photos to play on a 3 sec delay

  Scenario: Photo gallery stops the photos of the vehicle
    Given the photo gallery has been playing the photos on a 3 sec delay
    And the photo gallery has been displayed the link Slideshow with the pause icon
    When the buyer click on the Slideshow link
    Then the photo gallery trigger the photos to stop

  Scenario: The icons toggle from the play icon to the pause icon
    Given the photo gallery has been displayed the link Slideshow with the play icon
    When the buyer click on the Slideshow link
    Then the slideshow icon link toggle to the pause icon