#encoding: UTF-8
Feature: ID: 156059575 Name: Photo Gallery: No photos to show and blocked links

Background: User is Logged In
  Given the user has been logged in as a buyer

Scenario: Gray Camera is displayed for vehicle with no photos available
  Given a web browser is on "https://www.edgepipeline.com/components/vehicle/detail/aaanorthhouston/293394?btr=%2Fcomponents%2Freport%2Fbuy_now%2Fview%2Fall%23vehicle_708_243246" page
  When no photos are available for the vehicle
  Then a Gray Camera image is displayed as main picture
  And 'Fullscreen', 'Slideshow' and 'Download Pic' links are disabled