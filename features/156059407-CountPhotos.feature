@buyer_log_out
Feature: ID: 156059407 Name: Photo Gallery: Count of photos

  Background:
    Given the user has been logged in as a buyer

  Scenario: Display the current photos numeric order and the total number of photos
    Given the buyer has visited a detail vehicle page with the 'https://www.edgepipeline.com/components/vehicle/detail/abcbirmingham/319097' url
    And the seller of the vehicle has uploaded at least one photo for the vehicle
    When the photo gallery is loaded
    Then the photo gallery shows the current photo numeric order
    And the photo gallery shows the total number of photos
    And the numeric order and the total number of photos are separated with the word 'of'

  Scenario: Display '0 of 0' when there are not photos
    Given the buyer has visited a detail vehicle page with the 'https://www.edgepipeline.com/components/vehicle/detail/indianaaa/280425' url
    And the seller of the vehicle hasnt uploaded any photo
    When the photo gallery is loaded
    Then the photo count shows '0 of 0'

  Scenario: Increment of the current photo numeric order
     Given the buyer has visited a detail vehicle page with the 'https://www.edgepipeline.com/components/vehicle/detail/abcbirmingham/319097' url
     And the photo gallery has been showing a thumbnail for each picture
     When the buyer clicks on the second thumbnail
     Then the photo gallery shows the current photo numeric order


 