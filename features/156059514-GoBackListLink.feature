# encoding: UTF-8
Feature: ID: 156059514 Name: "Go Back to List" link

Background: User is Logged In
  Given the user has been logged in as a buyer

Scenario: 'Go Back to List' link appears
  Given a web browser is on "https://www.edgepipeline.com/components/vehicle/detail/akronaa/321595?btr=%2Fcomponents%2Freport%2Fpresale%2Fview%2Fakronaa-all%2F2018%2F5%2F1%23vehicle_25730_153418" page
  When the "btr" query string is in the page url
  Then the "Go Back to List" link appears
  And the "Go Back to List" link have the 'btr' query string value as its href attr

Scenario: 'Go Back to List' link does not appear
  Given a web browser is on "https://www.edgepipeline.com/components/vehicle/detail/akronaa/321595" page
  When the "btr" query string is not in the page url
  Then the "Go Back to List" does not appear