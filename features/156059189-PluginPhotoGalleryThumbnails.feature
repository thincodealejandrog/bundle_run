@buyer_log_out @working_on
Feature: 156059189 - Photo Gallery - Plugin for photo gallery and thumbnails

	Background:

    	Given the user has been logged in as a buyer
		And the buyer has visited a detail vehicle page with the "https://www.edgepipeline.com/components/vehicle/detail/abcbirmingham/319097" url
	
	Scenario: Main picture is loaded

		Then the vehicle main picture should be displayed with src attribute of "https://file3.autolookout.net/35117/007866/4495/O3Jlc29sdXRpb246MTYwMHgxMjAwOw==/stk_319097.jpg"

	Scenario: One thumbnail for each picture that was received for the vehicle

		Given pictures were provided
		Then a thumbnail for each picture is shown

	Scenario: Clicking on the picture should open the picture to full-screen

		Given pictures were provided
		When the buyer clicks on the main picture
		Then the main picture is open to fullscreen