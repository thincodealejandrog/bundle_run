#!/bin/bash
gem install bundler
bundle install --path vendor/bundle --without test

bundle exec 'cucumber -f html -o ./report.html'
